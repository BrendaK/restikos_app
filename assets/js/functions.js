var apiUrl = "http://brenda.1click.pf/RESTIKO/API/public/api/";

function getAllRestikos(page){
    if(page!=0) {
        $.ajax({
            url: apiUrl + "restikos?page=" + page,
            method: "get",
            success: function(resp){
                $("#restikosList").html("");
                console.log("resp", resp.data);
    
                resp.data.forEach(restiko => {
                    //traitemment et formatage de created_at format FR
                    var tmp = restiko.created_at.substring(0,10);
                    // var tmp = restiko.created_at.substring(0,10);
                    var date = tmp.split("-");
                    // var dateFr = date[2] + "/" + date[1] + "/" + date[0];
    
                    // console.log("tmp" tmp);
                    // console.log("date" date);
                    // console.log("my new date" date[2] + "/" + date[1] + "/" + date[0]);
    
                    var newRestiko = restikoCardTemplate;

                    newRestiko = newRestiko.replaceAll("###ID###", restiko.id);
                    newRestiko = newRestiko.replaceAll("###DATE###", restiko.date);
                    // newRestiko = newRestiko.replace("###DATE###", dateFr);
                    
                    newRestiko = newRestiko.replace("###STAR1###", (restiko.mood >= 1) ? "fas" : "far");
                    newRestiko = newRestiko.replace("###STAR2###", (restiko.mood >= 2) ? "fas" : "far");
                    newRestiko = newRestiko.replace("###STAR3###", (restiko.mood >= 3) ? "fas" : "far");
                    newRestiko = newRestiko.replace("###STAR4###", (restiko.mood >= 4) ? "fas" : "far");
                    newRestiko = newRestiko.replace("###STAR5###", (restiko.mood >= 5) ? "fas" : "far");

                    showModalRestiko(restiko.id);
                    $("#restikosList").append(newRestiko);
                    
                });
                generatePaginate(resp.links, page);
            }
        })
    }
}

function generatePaginate(paginateList, page){
    $("#paginateList").html("");
    paginateList.forEach(element => {

        var newLi = generateLiTemplate;
        
        newLi = newLi.replace("###PAGENAME###", element.label);
        var pageNumber = 0;

        if(element.url){
            var tmp = element.url.split("=");
            pageNumber = tmp[1];
        }

        var isActive = (pageNumber == page) ? "active" : "";

        newLi = newLi.replaceAll("###PAGENUMBER###", pageNumber);
        newLi = newLi.replaceAll("###ACTIVE###", isActive);

        $("#paginateList").append(newLi);

    })
}

function showModalRestiko(id){
    $.ajax({
        url: apiUrl + "restikos/" + id,
        method: "get",
        success: function(resp){
            console.log("resp in modal"  +id, resp);
            var showRestiko = modalRestiko;
            showRestiko = showRestiko.replace("###ID###", id);
            showRestiko = showRestiko.replace("###DO###", resp.do);
            showRestiko = showRestiko.replace("###LEARNED###", resp.learned);
            showRestiko = showRestiko.replace("###LOVED###", resp.loved);
            showRestiko = showRestiko.replace("###NEW###", resp.new);
            showRestiko = showRestiko.replace("###PROBLEMS###", resp.problems);
            showRestiko = showRestiko.replace("###OBJECTIVES###", resp.objectives);
            showRestiko = showRestiko.replace("###MISSED###", resp.missed);
            showRestiko = showRestiko.replace("###TRAINER###", resp.trainer);
            showRestiko = showRestiko.replace("###SUCCESS###", resp.success);
            showRestiko = showRestiko.replace("###TODO###", resp.todo);

            $("#showMyRestiko").append(showRestiko);
        }
    })
}
