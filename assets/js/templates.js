var restikoCardTemplate = 
'<div class="col-lg-3 mb-3">'+
    '<div class="card text-center rounded pt-4 pb-4">'+
        '<div class="card-body">'+
            '###ID###<br/>'+
            '###DATE###<br/>'+
            '<span class=""><i class="###STAR1### fas fa-star text-warning"></i></span>'+
            '<span class=""><i class="###STAR2### far fa-star text-warning"></i></span>'+
            '<span class=""><i class="###STAR3### far fa-star text-warning"></i></span>'+
            '<span class=""><i class="###STAR4### far fa-star text-warning"></i></span>'+
            '<span class=""><i class="###STAR5### far fa-star text-warning"></i></span><br>'+
            // Button trigger modal
            '<button type="button" class="btn btn-outline-info btn-sm mt-3 mb-3" data-toggle="modal" data-target="#exampleModal###ID###">'+
            'Consulter'+
            '</button>'+
        '</div>'+
    '</div>'+
'</div>';

var generateLiTemplate = '<li onclick="getAllRestikos(###PAGENUMBER###);" class="page-item ###ACTIVE###"><a class="page-link" href="#">###PAGENUMBER###</a></li>';

var modalRestiko = 
// Modal
'<div class="modal fade" id="exampleModal###ID###" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">'+
    '<div class="modal-dialog" role="document">'+
      '<div class="modal-content">'+
        '<div class="modal-header">'+
          '<h5 class="modal-title" id="exampleModalLabel">Restiko</h5>'+
          '<button type="button" class="close" data-dismiss="modal" aria-label="Close">'+
            '<span aria-hidden="true">&times;</span>'+
          '</button>'+
        '</div>'+
        '<div class="modal-body">'+
        '<h6>Ce que j\'ai fais</h6>'+
        '###DO###<br/>'+
        '<h6 class="mt-3">Ce que j\'ai appris</h6>'+
        '###LEARNED###<br/>'+
        '<h6 class="mt-3">Ce que j\'ai aimé</h6>'+
        '###LOVED###<br/>'+
        '<h6 class="mt-3">Ce que j\'ai utilisé de nouveaux</h6>'+
        '###NEW###<br/>'+
        '<h6 class="mt-3">Problèmes rencontrés</h6>'+
        '###PROBLEMS###<br/>'+
        '<h6 class="mt-3">Objectifs?</h6>'+
        '###OBJECTIVES###<br/>'+
        '<h6 class="mt-3">Ce qui m\'a manqué</h6>'+
        '###MISSED###<br/>'+
        '<h6 class="mt-3">A la place du formateur</h6>'+
        '###TRAINER###<br/>'+
        '<h6 class="mt-3">Objectifs atteints ou pas ?</h6>'+
        '###SUCCESS###<br/>'+
        '<h6 class="mt-3">Ce que je ferrai</h6>'+
        '###TODO###<br/>'+
        '</div>'+
        '<div class="modal-footer">'+
          '<button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>'+
        //   '<button type="button" class="btn btn-primary">Save changes</button>'+
        '</div>'+
      '</div>'+
    '</div>'+
  '</div>';
            
           